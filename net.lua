--[[
	MinuxCC Networking Library
]]

local net = {}

local openports = {}

function net.openport(port)
	if not openports[port] then
		openports[port]=true
		net.modem.open(port)
	end
end

function net.closeport(port)
	if openports[port] then
		openports[port]=nil
		net.modem.close(port)
	end
end

function net.send(msg, ip, port, replyport)
	if replyport==nil then
		replyport = port
	end
	if openports[port] then
		net.modem.transmit(port, replyport, {msg, ip, net.ip})
		return true
	else
		return false
	end
end

-- a debug function, not recommended for actual networking applications
function net.listen(port)
	if openports[port] then
		while true do
			local e = {os.pullEvent("modem_message")}
			if e[3]==port then
				return e
			end
		end
	else
		net.openport(port)
		net.listen(port)
		net.closeport(port)
	end
end

function net.init(side)
	net.modem = side==nil and peripheral.find("modem") or peripheral.wrap(side)
	net.side = side
end

return net
