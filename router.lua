--[[
	MinuxCC Compliant Router application
]]
local lan = require("lan")
local net = require("net")

local ip_pool = {}

local port_forward = {}

function is_in(table, value)
	for _,i in pairs(table) do
		if i==value then
			return true
		end
	end
	return false
end

function removeIP(ip)
	for i = 1, #ip_pool do
		if ip_pool[i]==ip then
			ip_pool[i]=nil
		end
	end
end


function genGlobalIP(set_static)
	math.randomseed(os.time())
	net.ip=math.random(10000,99999)
	if set_static then
		local file = fs.open("static_ip", "w")
		file.write(string.format("%x", net.ip))
		file.close()
	end
end

function init(online, static, public, lcl)
	lan.setIP(1)
	lan.init(lcl)
	table.insert(ip_pool, lan.ip)
	if online then
		if (fs.exists("static_ip")) then
			local file = fs.open("static_ip", "r")
			net.ip = tonumber(file.readLine(), 16)
			file.close()
		else
			genGlobalIP(static==nil and false or static)
		end
		net.init(public)
	end
end

function listen()
	e = {os.pullEvent()}
	if e[1]=="modem_message" then
		lan.send(e[3], e[4], e[5])
	end
end

function main()
	init(true, true, "top", "back")
	print(lan.ip, net.ip)
	while true do
		local msg = lan.listen(1)
		if msg[5]==1 then
			local ip = 100+#ip_pool
			lan.send(2,1,ip)
			table.insert(ip_pool, ip)
		if msg[5]==1 then --LAN IP Request
			local n_ip = true
			if #ip_pool>1 then --Check for empty slots in the ip_pool
				local last_ip = 100
				for i = 2, #ip_pool do
					if ip_pool[i]==nil then
						ip_pool[i]=last_ip+1
						local ip = ip_pool[i]
						lan.send(ip,ip,2,1)
						n_ip = false
						break
					else
						last_ip=ip_pool[i]
					end
				end
			end
			if n_ip then --If no empty slots in ip_pool were found, make a new one
				local ip = 100+#ip_pool
				print(lan.send(ip,ip,2,1))
				table.insert(ip_pool, ip)
			end
			print(textutils.serialise(ip_pool))
		elseif msg[5][1]==2 then --LAN IP Removal Request
			print("ip remove request")
			removeIP(msg[5][3])
		elseif msg[5][1]==0 then --LAN NetSend Request
			if is_in(ip_pool, msg[5][2]) then
				lan.send(msg[5][2], msg[5][3], msg[5][4], msg[5][5]) --Redirect message over LAN
			else
				net.send(msg[5][2], msg[5][3], msg[5][4], msg[5][5]) --Redirect message over NET
			end
		elseif msg[5][1]==3 then --LAN Open Router Port Request

		elseif msg[5][1]==4 then --LAN Close Router Port Request

		end
	end
end

main()
