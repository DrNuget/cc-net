--[[
	MinuxCC LAN library
]]
local lan = {}
local net = require("net")
setmetatable(lan, {__index = net})

function lan.setIP(ip)
	lan.ip=ip
	net.ip=ip
end

function lan.ipRequest() --Request an IP address from the router
    lan.modem.transmit(1,1,1)
    local listening = true
    while listening do
		print("wew")
        local msg = lan.listen(2)
        if msg[5][3]==1 then
			lan.setIP(msg[5][1])
            listening = false
        end
    end
end

function lan.leaveNetwork()
    lan.send(2,1,1,2)
end

function lan.netSend(msg, ip, port, replyport) --Send message to a specified ip through the router, not to be confused with net.send()
	lan.send({0,msg,ip,port,replyport}, 1, 1, 2)
end

function lan.openRouterPort(port)
	lan.send({3, port}, 1, 1, 2)
end

function lan.closeRouterPort(port)
	lan.send({4, port}, 1, 1, 2)
end

function lan.init(side)
    net.init(side)
    lan.openport(1)
    lan.openport(2)
end

return lan
